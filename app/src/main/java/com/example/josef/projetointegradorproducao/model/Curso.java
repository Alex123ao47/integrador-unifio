package com.example.josef.projetointegradorproducao.model;

public class Curso {

    private String nomeCurso;
    private String nota;
    private String coordenador;

    public Curso(){

    }

    public Curso(String nomeCurso, String nota, String coordenador) {
        this.nomeCurso = nomeCurso;
        this.nota = nota;
        this.coordenador = coordenador;
    }

    public String getNomeCurso() {
        return nomeCurso;
    }

    public void setNomeCurso(String nomeCurso) {
        this.nomeCurso = nomeCurso;
    }

    public String getNota() {
        return nota;
    }

    public void setNota(String nota) {
        this.nota = nota;
    }

    public String getCoordenador() {
        return coordenador;
    }

    public void setCoordenador(String coordenador) {
        this.coordenador = coordenador;
    }
}
