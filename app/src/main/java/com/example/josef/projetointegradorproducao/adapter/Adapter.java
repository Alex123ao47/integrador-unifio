package com.example.josef.projetointegradorproducao.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.josef.projetointegradorproducao.R;
import com.example.josef.projetointegradorproducao.model.Curso;

import java.util.List;

public class Adapter extends RecyclerView.Adapter<Adapter.MyViewHolder> {

    private List<Curso> listaCursos;

    public Adapter(List <Curso> lista) {

        this.listaCursos = lista;

    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View itemLista = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.fragment_adapter_lista,viewGroup,false);



        return new MyViewHolder (itemLista);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int position) {

        Curso curso = listaCursos.get(position);
        myViewHolder.curso.setText(curso.getNomeCurso());
        myViewHolder.coordenador.setText(curso.getCoordenador());
        myViewHolder.nota.setText(curso.getNota());

    }

    @Override
    public int getItemCount() {

        return listaCursos.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{

        TextView curso;
        TextView nota;
        TextView coordenador;


        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            curso = itemView.findViewById(R.id.textCurso);
            nota = itemView.findViewById(R.id.textNota);
            coordenador = itemView.findViewById(R.id.textCoordenador);
        }
    }

}

