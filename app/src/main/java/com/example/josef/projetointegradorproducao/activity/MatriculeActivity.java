package com.example.josef.projetointegradorproducao.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.josef.projetointegradorproducao.R;
import com.example.josef.projetointegradorproducao.dao.DAOAluno;
import com.example.josef.projetointegradorproducao.model.Aluno;
import com.example.josef.projetointegradorproducao.util.BancoUtil;

public class MatriculeActivity extends Activity {

    private DAOAluno daoAluno;
    private EditText nome;
    private EditText email;
    private EditText telefone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_matricule);

        BancoUtil.startBD(MatriculeActivity.this, "bd_fio");

        daoAluno = new DAOAluno();
    }

    public void realizarInscricao(View v) {
        nome = findViewById(R.id.etNome);
        email = findViewById(R.id.etEmail);
        telefone = findViewById(R.id.etTelefone);
        boolean validar;

        Aluno aluno = new Aluno(nome.getText().toString(), email.getText().toString(), telefone.getText().toString());

        validar = validarCampos(aluno);

        if (validar == true) {
            daoAluno.inserir(aluno);
            Toast.makeText(this, "Inscrição realizada com sucesso!", Toast.LENGTH_LONG).show();
            limparCampos();
        } else {
            Toast.makeText(this, "É necessário preencher todos os campos!", Toast.LENGTH_SHORT).show();
        }
    }

    public void limparCampos() {
        nome.setText("");
        email.setText("");
        telefone.setText("");
    }

    public boolean validarCampos(Aluno aluno) {
        if (aluno.getNome().isEmpty() || aluno.getEmail().isEmpty() || aluno.getTelefone().isEmpty()) {
            return false;
        } else {
            return true;
        }
    }

    public void voltar(View view) {
        Intent i = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(i);

        finish();
    }
}
