package com.example.josef.projetointegradorproducao.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentTransaction;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.FrameLayout;

import com.example.josef.projetointegradorproducao.R;
import com.example.josef.projetointegradorproducao.fragment.EadFragment;
import com.example.josef.projetointegradorproducao.fragment.EventosFragment;
import com.example.josef.projetointegradorproducao.fragment.ExtensaoFragment;
import com.example.josef.projetointegradorproducao.fragment.FundoInicialFragment;
import com.example.josef.projetointegradorproducao.fragment.PosGraducaoFragment;
import com.example.josef.projetointegradorproducao.fragment.UnifioFragment;
import com.example.josef.projetointegradorproducao.util.BancoUtil;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private FrameLayout frameLayout;

    private BancoUtil banco;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

         BancoUtil.startBD(MainActivity.this, "bd_fio");

        frameLayout = findViewById(R.id.frameContainer);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FundoInicialFragment fundo = new FundoInicialFragment();  //INICIANDO PRIMEIRO O FUNDO BRANCO COM O LOGO
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frameContainer, fundo);
        fragmentTransaction.commit();

//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                enviarEmail();
//            }
//        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_conhecaAsUnifio) {

            UnifioFragment unifio = new UnifioFragment();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.frameContainer, unifio);
            fragmentTransaction.commit();

        } else if (id == R.id.nav_graduacao) {

            Intent acao1 = new Intent(getApplicationContext(), Main2Activity.class);
            startActivity(acao1);
            finish();

        } else if (id == R.id.nav_posGraduacao) {

            PosGraducaoFragment posgraduacao = new PosGraducaoFragment();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.frameContainer, posgraduacao);
            fragmentTransaction.commit();

        } else if (id == R.id.nav_extensao) {

            ExtensaoFragment extensao = new ExtensaoFragment();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.frameContainer, extensao);
            fragmentTransaction.commit();

        } else if (id == R.id.nav_ead) {

            EadFragment ead = new EadFragment();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.frameContainer, ead);
            fragmentTransaction.commit();

        } else if (id == R.id.nav_eventos) {

            EventosFragment eventos = new EventosFragment();
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.replace(R.id.frameContainer, eventos);
            fragmentTransaction.commit();


        } else if (id == R.id.nav_agendado) {
            Intent i = new Intent(getApplicationContext(), MatriculeActivity.class);
            startActivity(i);

            finish();

        }
        else if (id == R.id.nav_inscritos) {
            Intent i = new Intent(getApplicationContext(), ListaInscritosActivity.class);
            startActivity(i);

            finish();

        }
        else if (id == R.id.nav_portaldoAluno) {
            String url = "http://www.fio.edu.br/aluno";
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(url));
            startActivity(i);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void enviarEmail() {

        Intent email = new Intent(Intent.ACTION_SEND);
        email.putExtra(Intent.EXTRA_EMAIL, new String[]{"josefioedubr@gmail.com"});
        email.putExtra(Intent.EXTRA_SUBJECT, "Contato pelo App");
        email.putExtra(Intent.EXTRA_TEXT, "Mensagem automática");

        //configurar apps para e-mail
        email.setType("message/rfc822");

        startActivity(Intent.createChooser(email, "Escolha o App de e-mail"));

    }
}



