package com.example.josef.projetointegradorproducao.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.josef.projetointegradorproducao.R;
import com.example.josef.projetointegradorproducao.adapter.ListaInscritosAdapter;
import com.example.josef.projetointegradorproducao.dao.DAOAluno;
import com.example.josef.projetointegradorproducao.helper.RecyclerItemClickListener;
import com.example.josef.projetointegradorproducao.model.Aluno;

import java.util.ArrayList;
import java.util.List;

public class ListaInscritosActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private TextView txtQtdInscritos;
    private ListaInscritosAdapter listaInscritosAdapter;
    private List<Aluno> listaAluno = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_inscritos);

        recyclerView = findViewById(R.id.recyclerLista);

        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(
                        getApplicationContext(),
                        recyclerView,
                        new RecyclerItemClickListener.OnItemClickListener() {
                            @Override
                            public void onItemClick(View view, int position) {
                                Log.i("clique", "onItemClick");
                            }

                            @Override
                            public void onLongItemClick(View view, int position) {
                                Log.i("clique", "onLongItemClick");
                            }

                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                            }
                        }
                )
        );

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                startActivity(new Intent(this, MainActivity.class));
                finish();
                break;
            default:
                break;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(this, MainActivity.class));
        finishAffinity();
        return;
    }

    public void carregarInscritos() {

        DAOAluno daoAluno = new DAOAluno();
        listaAluno = daoAluno.consultarInscritos();

        listaInscritosAdapter = new ListaInscritosAdapter(listaAluno);

        RecyclerView.LayoutManager layoutManagerInscritos = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManagerInscritos);
        recyclerView.setHasFixedSize(true);
        recyclerView.addItemDecoration(new DividerItemDecoration(getApplicationContext(), LinearLayout.VERTICAL));
        recyclerView.setAdapter(listaInscritosAdapter);

        txtQtdInscritos = findViewById(R.id.txtQtdInscritos);

        txtQtdInscritos.setText("Quantidade de Inscritos: " + String.valueOf(listaInscritosAdapter.getItemCount()));
    }

    @Override
    protected void onStart() {
        carregarInscritos();
        super.onStart();
    }
}
