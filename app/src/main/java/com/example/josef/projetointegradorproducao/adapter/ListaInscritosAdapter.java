package com.example.josef.projetointegradorproducao.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.josef.projetointegradorproducao.R;
import com.example.josef.projetointegradorproducao.model.Aluno;

import java.util.List;

public class ListaInscritosAdapter extends RecyclerView.Adapter<ListaInscritosAdapter.MyViewHolder> {

    private List<Aluno> listaInscritos;

    public ListaInscritosAdapter(List<Aluno> lista)  {
        this.listaInscritos = lista;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View itemLista = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.lista_inscritos_adapter, viewGroup, false);

        return new MyViewHolder(itemLista);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {

        Aluno aluno = listaInscritos.get(i);
        myViewHolder.nome.setText(aluno.getNome());
        myViewHolder.telefone.setText(aluno.getTelefone());
        myViewHolder.email.setText(aluno.getEmail());

    }

    @Override
    public int getItemCount() {
        return this.listaInscritos.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView nome;
        TextView telefone;
        TextView email;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            nome = itemView.findViewById(R.id.txtNome);
            telefone = itemView.findViewById(R.id.txtTelefone);
            email = itemView.findViewById(R.id.txtEmail);
        }
    }
}
