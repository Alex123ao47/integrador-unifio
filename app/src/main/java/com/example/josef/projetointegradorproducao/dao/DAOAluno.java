package com.example.josef.projetointegradorproducao.dao;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.example.josef.projetointegradorproducao.model.Aluno;
import com.example.josef.projetointegradorproducao.util.BancoUtil;

import java.util.ArrayList;
import java.util.List;

public class DAOAluno {

    private SQLiteDatabase bancoDados;
    private Cursor cursor;

    public DAOAluno() {
        bancoDados = BancoUtil.BANCO_DADOS;
    }

    public void inserir(Aluno aluno) {
        try {
            bancoDados.execSQL("INSERT INTO cad_inscritosVestibular (nome, email, telefone)  VALUES ('" + aluno.getNome() + "', '" + aluno.getEmail() + "','" + aluno.getTelefone() + "')");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Aluno> consultarInscritos() {
        List<Aluno> listaAluno = new ArrayList<>();
        try {
            cursor = bancoDados.rawQuery("SELECT nome, email, telefone FROM cad_inscritosVestibular", null);

            while (cursor.moveToNext()) {
                Aluno aluno;
                aluno = new Aluno(cursor.getString(cursor.getColumnIndex("nome")),
                        cursor.getString(cursor.getColumnIndex("email")),
                        cursor.getString(cursor.getColumnIndex("telefone")));

                listaAluno.add(aluno);
            }
            return listaAluno;
        } catch (Exception e) {
            e.printStackTrace();
            return listaAluno;
        }
    }
}
