package com.example.josef.projetointegradorproducao.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;

import com.example.josef.projetointegradorproducao.R;
import com.example.josef.projetointegradorproducao.adapter.Adapter;
import com.example.josef.projetointegradorproducao.model.Curso;

import java.util.ArrayList;
import java.util.List;

public class Main2Activity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private List<Curso> listaCursos = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);


        recyclerView = findViewById(R.id.recyclerView);


        //criar cursos
        this.criarCursos();

        //Configurar adapter
        Adapter adapter = new Adapter(listaCursos);


        //Configurar Recyclerview
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
    }

    public void criarCursos(){

        Curso curso = new Curso("Administração","","Prof.Paulo Celso Francisco");
        listaCursos.add(curso);

        curso = new Curso("Agronomia - Integral","","Prof.Renato Maravalhas");
        listaCursos.add(curso);

        curso = new Curso("Agronomia - Noturno","","Prof.Renato Maravalhas");
        listaCursos.add(curso);

        curso = new Curso("Arquitetura e Urbanismo","","Prof.Elizabeth Mie Arakaki");
        listaCursos.add(curso);

        curso = new Curso("Artes Visuais","","Prof.Ana Luiza Bernardo Guimarães");
        listaCursos.add(curso);

        curso = new Curso("Biomeducina","","Prof.Luciano Gatti");
        listaCursos.add(curso);

        curso = new Curso("Ciências Biológicas","","Prof.Odair Francisco");
        listaCursos.add(curso);

        curso = new Curso("Ciências Contábeis","","Prof.Wainer Albanez");
        listaCursos.add(curso);

        curso = new Curso("Direito","","Prof.Adriano Aranão");
        listaCursos.add(curso);

        curso = new Curso("Enfermagem","","Prof.Juliano Rodrigues Coimbra");
        listaCursos.add(curso);

        curso = new Curso("Engenharia Civil","","Prof.Barbara Gonçalves Logullo ");
        listaCursos.add(curso);

        curso = new Curso("Engenharia Elétrica","","Prof.Luiz Adriano Galan Madalena ");
        listaCursos.add(curso);

        curso = new Curso("Engenharia Mecânica","","Prof.Dário de Almeida Jané ");
        listaCursos.add(curso);

        curso = new Curso("Engenharia de Produção","","Prof.Dário de Almeida Jané ");
        listaCursos.add(curso);

        curso = new Curso("Farmácia","","Prof.Cristiane Fátima Guarido ");
        listaCursos.add(curso);

        curso = new Curso("Farmácia","","Prof.Cristiane Fátima Guarido ");
        listaCursos.add(curso);

        curso = new Curso("Fisioterapia","","Prof.Julio Agante");
        listaCursos.add(curso);

        curso = new Curso("Medicina Veterinária","","Prof.Claudia Yumi");
        listaCursos.add(curso);

        curso = new Curso("Nutrição","","Prof.Luciana Luiggi Teixeira");
        listaCursos.add(curso);

        curso = new Curso("Odontologia","","Prof.Cintia Maria de Souza e Silva");
        listaCursos.add(curso);

        curso = new Curso("Pedagogia","","Prof.Flávia Cristina Oliveira");
        listaCursos.add(curso);

        curso = new Curso("Psicologia","","Prof.Fábio Sagula de Oliveira");
        listaCursos.add(curso);

        curso = new Curso("Sistemas de Informação","","Prof.Rogério Marinke");
        listaCursos.add(curso);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                startActivity(new Intent(this, MainActivity.class));
                finish();
                break;
            default:
                break;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(this, MainActivity.class));
        finishAffinity();
        return;
    }

}
