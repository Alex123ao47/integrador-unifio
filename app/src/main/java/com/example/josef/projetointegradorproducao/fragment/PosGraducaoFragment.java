package com.example.josef.projetointegradorproducao.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.example.josef.projetointegradorproducao.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class PosGraducaoFragment extends Fragment {


    public PosGraducaoFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_pos_graducao, container, false);
    }

}