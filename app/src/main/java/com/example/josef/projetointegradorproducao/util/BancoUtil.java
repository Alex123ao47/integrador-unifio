package com.example.josef.projetointegradorproducao.util;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

public class BancoUtil {

    public static SQLiteDatabase BANCO_DADOS;

    public static boolean startBD(Context ctx, String nomeBanco) {
        try {
            BANCO_DADOS = ctx.openOrCreateDatabase(nomeBanco, Context.MODE_PRIVATE, null);
            String script = "CREATE TABLE IF NOT EXISTS cad_inscritosVestibular (" +
                    "nome VARCHAR(45) NOT NULL, " +
                    "email VARCHAR(45)," +
                    "telefone VARCHAR(15))";

            BANCO_DADOS.execSQL(script);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private void closeBD() {
        try {
            BANCO_DADOS.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
